import { PixelRatio } from 'react-native'

let arrowLeft = {}
let arrowRight = {}
let backIcon = {}
let menuIcon = {}
let starIcon = {}
let starFullIcon = {}

/**
 * Carregamento do ícones a partir da pasta assets
 * de acordo o dpi
 *
 * Obs: Tentei encontrar uma forma menos verbosa de fazer isso,
 * mas infelizmente não encontrei. Agradeço o feedback
 * se houver uma maneira melhor
 */

if (PixelRatio.get() < 1.5) {
  arrowLeft = require('../assets/mdpi/arrow_left.png')
  arrowRight = require('../assets/mdpi/arrow_right.png')
  backIcon = require('../assets/mdpi/ic_back.png')
  menuIcon = require('../assets/mdpi/ic_menu.png')
  starIcon = require('../assets/mdpi/ic_star.png')
  starFullIcon = require('../assets/mdpi/ic_starfull.png')
} else if (PixelRatio.get() >= 1.5 && PixelRatio.get() < 2) {
  arrowLeft = require('../assets/hdpi/arrow_left.png')
  arrowRight = require('../assets/hdpi/arrow_right.png')
  backIcon = require('../assets/hdpi/ic_back.png')
  menuIcon = require('../assets/hdpi/ic_menu.png')
  starIcon = require('../assets/hdpi/ic_star.png')
  starFullIcon = require('../assets/hdpi/ic_starfull.png')
} else if (PixelRatio.get() >= 2 && PixelRatio.get() < 3) {
  arrowLeft = require('../assets/xhdpi/arrow_left.png')
  arrowRight = require('../assets/xhdpi/arrow_right.png')
  backIcon = require('../assets/xhdpi/ic_back.png')
  menuIcon = require('../assets/xhdpi/ic_menu.png')
  starIcon = require('../assets/xhdpi/ic_star.png')
  starFullIcon = require('../assets/xhdpi/ic_starfull.png')
} else if (PixelRatio.get() >= 3 && PixelRatio.get() < 3.5) {
  arrowLeft = require('../assets/xxhdpi/arrow_left.png')
  arrowRight = require('../assets/xxhdpi/arrow_right.png')
  backIcon = require('../assets/xxhdpi/ic_back.png')
  menuIcon = require('../assets/xxhdpi/ic_menu.png')
  starIcon = require('../assets/xxhdpi/ic_star.png')
  starFullIcon = require('../assets/xxhdpi/ic_starfull.png')
} else if (PixelRatio.get() >= 3.5) {
  arrowLeft = require('../assets/xxxhdpi/arrow_left.png')
  arrowRight = require('../assets/xxxhdpi/arrow_right.png')
  backIcon = require('../assets/xxxhdpi/ic_back.png')
  menuIcon = require('../assets/xxxhdpi/ic_menu.png')
  starIcon = require('../assets/xxxhdpi/ic_star.png')
  starFullIcon = require('../assets/xxxhdpi/ic_starfull.png')
}

export { arrowLeft, arrowRight, backIcon, menuIcon, starIcon, starFullIcon }
