import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    height: 60,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 15,
    marginTop: 30,
    borderColor: '#eee',
    borderBottomWidth: 1
  },
  title: {
    fontSize: 22,
    color: '#EC008C',
    fontFamily: 'Montserrat-ExtraLight',
    padding: 5
  },
  button: {
    marginTop: 6,
    paddingHorizontal: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  icon: {
    width: 25,
    resizeMode: 'contain',
    marginRight: 15,
    alignSelf: 'center'
  }
})
