import React from 'react'
import PropTypes from 'prop-types'
import { Image, Text, TouchableOpacity, View } from 'react-native'

import { menuIcon } from '../../services/loadIcons'
import styles from './styles'

const Header = ({ title, ...props }) => (
  <View style={styles.container}>
    <TouchableOpacity style={styles.button} {...props}>
      <Image style={styles.icon} source={menuIcon} />
    </TouchableOpacity>
    <Text style={styles.title}>{title}</Text>
  </View>
)

Header.propTypes = {
  title: PropTypes.string.isRequired
}

export { Header }
