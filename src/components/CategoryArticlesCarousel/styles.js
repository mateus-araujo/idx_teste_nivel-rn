import { Dimensions, StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
  backgroundImage: {
    backgroundColor: 'rgba(0,0,0,.6)',
    flex: 1
  },
  itemImage: {
    height: 200,
    width: Dimensions.get('window').width
  },
  itemTitle: {
    color: '#FFFFFF',
    fontSize: 20,
    fontFamily: 'Montserrat-Regular',
    position: 'absolute',
    top: '60%',
    left: 0,
    paddingHorizontal: 15
  },
  itemDescription: {
    color: '#FFFFFF',
    fontSize: 15,
    fontFamily: 'Montserrat-ExtraLight',
    position: 'absolute',
    top: '75%',
    left: 0,
    paddingHorizontal: 15
  },
  containerButtons: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 15
  },
  buttonBack: {
    padding: 15,
    width: 25,
    alignItems: 'center'
  },
  iconBack: {
    width: 10,
    resizeMode: 'contain',
    marginRight: 10
  },
  buttonForward: {
    padding: 15,
    width: 25,
    alignItems: 'center'
  },
  iconForward: {
    width: 10,
    resizeMode: 'contain',
    marginLeft: 10
  },
  openButton: {
    height: '100%',
    width: '75%',
    justifyContent: 'center',
    marginHorizontal: 10
  }
})
