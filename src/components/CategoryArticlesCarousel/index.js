/**
 * Carousel usado no ListCategory e ListFavorite
 */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Dimensions,
  Image,
  ImageBackground,
  ScrollView,
  Text,
  TouchableOpacity,
  View
} from 'react-native'

import { arrowLeft, arrowRight } from '../../services/loadIcons'
import styles from './styles'

class CategoryArticlesCarousel extends Component {
  state = {
    screenWidth: Dimensions.get('window').width
  }

  scrollBack = id => {
    const xPos = this.state.screenWidth * (id - 1)
    this.scroll.scrollTo({ x: xPos, y: 0, animated: true })
  }

  scrollForward = id => {
    const xPos = this.state.screenWidth * (id + 1)
    this.scroll.scrollTo({ x: xPos, y: 0, animated: true })
  }

  // Renderiza o item, somente background e botões
  renderItem = (item, id) => (
    <ImageBackground
      key={id}
      source={{ uri: item.galery[0] }}
      style={styles.itemImage}
    >
      <View style={styles.backgroundImage}>
        <Text style={styles.itemTitle}>{item.title.toUpperCase()}</Text>
        <Text
          style={styles.itemDescription}
          numberOfLines={2}
          ellipsizeMode={'tail'}
        >
          {item.description}
        </Text>
        <View style={styles.containerButtons}>
          {/* Renderiza o botão somente se não for o primeiro do array */}
          {id !== 0 ? (
            <TouchableOpacity
              style={styles.buttonBack}
              onPress={() => this.scrollBack(id)}
            >
              <Image style={styles.iconBack} source={arrowLeft} />
            </TouchableOpacity>
          ) :
            // View com mesmas estilizações do botão para não comprometer o layout
            <View style={styles.buttonBack} />
          }

          {/** Botão invísisel entre outros dois
           * botões para abertura do artigo
           */}
          <TouchableOpacity
            style={styles.openButton}
            onPress={() => this.props.openArticle(this.props.category, item)}
          />

          {/* Renderiza o botão somente se o item for o último do array */}
          {id < this.props.items.length - 1 ? (
            <TouchableOpacity
              style={styles.buttonForward}
              onPress={() => this.scrollForward(id)}
            >
              <Image style={styles.iconForward} source={arrowRight} />
            </TouchableOpacity>
          ) :
            // View com mesmas estilizações do botão para não comprometer o layout
            <View style={styles.buttonForward} />
          }
        </View>
      </View>
    </ImageBackground>
  )

  render() {
    return (
      <ScrollView
        horizontal
        pagingEnabled
        ref={scroll => this.scroll = scroll}
      >
        {this.props.items.map((item, id) => this.renderItem(item, id))}
      </ScrollView>
    )
  }
}

CategoryArticlesCarousel.propTypes = {
  category: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.object),
  openArticle: PropTypes.func,
  showOnlyFavorites: PropTypes.bool
}

export { CategoryArticlesCarousel }
