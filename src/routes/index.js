/**
 * Módulo de rotas
 */
import {
  createAppContainer,
  createDrawerNavigator,
  createStackNavigator
} from 'react-navigation'
import { Article, ListCategory, ListFavorite, Menu } from '../pages'
import { Animated, Easing } from 'react-native'

/**
 * Animação utilizada na abertura dos artigos
 */
const transitionAnimation = () => ({
  transitionSpec: {
    duration: 500,
    easing: Easing.out(Easing.poly(4)),
    timing: Animated.timing
  },
  screenInterpolator: sceneProps => {
    const { layout, position, scene } = sceneProps
    const { index } = scene

    const height = layout.initHeight
    const translateX = position.interpolate({
      inputRange: [index - 1, index, index + 1],
      outputRange: [height, 0, 0]
    })

    const opacity = position.interpolate({
      inputRange: [index - 1, index - 0.99, index],
      outputRange: [0, 1, 1]
    })

    return { opacity, transform: [{ translateX }] }
  }
})

const Routes = createAppContainer(
  createDrawerNavigator(
    {
      Article,
      ListCategory: createStackNavigator(
        {
          ListCategory,
          Article
        },
        {
          headerMode: 'none',
          cardOverlayEnabled: true,
          // Atribuição da animação na propriedade
          transitionConfig: transitionAnimation
        }
      ),
      ListFavorite: createStackNavigator(
        {
          ListFavorite,
          Article
        },
        {
          headerMode: 'none',
          cardOverlayEnabled: true,
          transitionConfig: transitionAnimation
        }
      )
    },
    {
      initialRouteName: 'ListCategory',
      // Componente de Menu customizado
      contentComponent: Menu
    }
  )
)

export default Routes
