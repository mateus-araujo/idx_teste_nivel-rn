import { Dimensions, StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
  container: {
    flex: 1
  },
  imageContainer: {},
  articleImage: {
    width: '100%',
    height: Dimensions.get('window').height / 3,
    resizeMode: 'center'
  },
  backgroundImage: {
    backgroundColor: 'rgba(0,0,0,.4)',
    flex: 1
  },
  textContainer: {
    padding: 15
  },
  articleTitle: {
    color: '#434343',
    fontSize: 25,
    fontFamily: 'Montserrat-Medium'
  },
  articleCategory: {
    color: '#EC008C',
    fontFamily: 'Montserrat-Light',
    fontSize: 15
  },
  articleDescription: {
    color: '#7B7C80',
    fontFamily: 'Montserrat-Light',
    fontSize: 19,
    marginTop: 20
  },
  buttonsContainer: {
    marginTop: 30,
    marginHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  backButton: {
    padding: 5,
    height: 75,
    alignItems: 'center'
  },
  backIcon: {
    width: 30,
    height: 75,
    resizeMode: 'contain'
  },
  starButton: {
    padding: 5,
    alignItems: 'center'
  },
  starIcon: {
    width: 30,
    height: 75,
    resizeMode: 'contain'
  },
  starFullIcon: {
    width: 30,
    height: 75,
    resizeMode: 'contain',
    tintColor: '#FFFFFF'
  }
})
