/**
 * Component do article
 */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  BackHandler,
  Image,
  ImageBackground,
  ScrollView,
  StatusBar,
  Text,
  TouchableOpacity,
  View
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { Creators as CategoriesActions } from '../../store/ducks/categories'

import { backIcon, starIcon, starFullIcon } from '../../services/loadIcons'

import styles from './styles'

class Article extends Component {
  static propTypes = {
    setFavorite: PropTypes.func
  }

  componentDidMount() {
    // Captura do evento do botão voltar do android
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      this.goBack()
      return true
    })
  }

  // Remoção do evento do botão voltar do android
  componentWillUnmount() {
    this.backHandler.remove()
  }

  goBack = async () => {
    StatusBar.setBarStyle('dark-content', true)
    this.props.navigation.goBack()
  }

  // Toggle do favorito
  setFavorite = (category, id) => {
    this.props.setFavorite(category, id)
  }

  render() {
    // Desestruturação dos parametros recebidos pela navegação
    const { category, article } = this.props.navigation.state.params
    const { id, title, description, galery, favorite } = article

    return (
      <View style={styles.container}>
        <View style={styles.imageContainer}>
          <ImageBackground
            source={{ uri: galery[0] }}
            style={styles.articleImage}
          >
            <View style={styles.backgroundImage}>
              <View style={styles.buttonsContainer}>
                {/* Botaão de voltar */}
                <TouchableOpacity
                  style={styles.backButton}
                  onPress={this.goBack}
                >
                  <Image style={styles.backIcon} source={backIcon} />
                </TouchableOpacity>

                {/**
                 * Botão de star
                 *
                 * Obs: Renderiza uma imagem diferente quando está favoritado
                 * */}
                <TouchableOpacity
                  style={styles.starButton}
                  onPress={() => this.setFavorite(category, id)}
                >
                  {favorite ?
                    <Image style={styles.starFullIcon} source={starFullIcon} />
                    :
                    <Image style={styles.starIcon} source={starIcon} />
                  }
                </TouchableOpacity>
              </View>
            </View>
          </ImageBackground>
        </View>

        <ScrollView contentContainerStyle={styles.textContainer}>
          <Text style={styles.articleTitle}>{title.toUpperCase()}</Text>
          <Text style={styles.articleCategory}>{category}</Text>
          <Text style={styles.articleDescription}>{description}</Text>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  state
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(CategoriesActions, dispatch)

const _Article = connect(
  mapStateToProps,
  mapDispatchToProps
)(Article)

export { _Article as Article }
