/**
 * Lista de favoritos
 */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ScrollView, StatusBar, Text, View } from 'react-native'
import { connect } from 'react-redux'

import { CategoryArticlesCarousel, Header } from '../../components'

import styles from './styles'

class ListFavorite extends Component {
  static propTypes = {
    categories: PropTypes.shape({
      listFavorite: PropTypes.array,
      hasFavorites: PropTypes.bool
    })
  }

  // Função de vericação de favoritos
  hasFavorites = () => this.props.categories.hasFavorites

  toggleDrawer = () => {
    StatusBar.setBarStyle('light-content', true)
    this.props.navigation.toggleDrawer()
  }

  openArticle = (category, item) => {
    StatusBar.setBarStyle('light-content', true)
    this.props.navigation.navigate('Article', {
      category,
      article: item
    })
  }

  renderCarousels = () =>
    this.props.categories.listFavorite.map((category, id) => (
      <CategoryArticlesCarousel
        key={id}
        category={category.category}
        items={category.items}
        openArticle={this.openArticle}
      />
    ))

  render() {
    return (
      <View style={styles.container}>
        <Header title="Favorites" onPress={this.toggleDrawer} />

        <ScrollView>
          {/* Verificação se existem favoritos, senão mostra-se a mensagem e vice-versa */}
          {!this.hasFavorites() ? (
            <Text style={styles.notFoundText}>
              Você não adicionou nenhum favorito ainda
            </Text>
          ) :
            this.renderCarousels()
          }
        </ScrollView>
      </View>
    )
  }
}

// Mapeamento das props do redux
const mapStateToProps = state => {
  const { categories } = state

  return { categories }
}

const _ListFavorite = connect(mapStateToProps)(ListFavorite)

export { _ListFavorite as ListFavorite }
