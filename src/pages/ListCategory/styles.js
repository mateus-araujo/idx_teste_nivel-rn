import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
  container: {
    flex: 1
  },
  notFoundText: {
    fontSize: 20,
    fontFamily: 'Montserrat-Light',
    marginTop: 10,
    paddingVertical: 5,
    paddingHorizontal: 15,
    textAlign: 'center'
  },
  categoryContainer: {
    backgroundColor: '#EC008C'
  },
  categoryText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: 'Montserrat-Regular',
    paddingVertical: 5,
    paddingHorizontal: 15
  }
})
