/**
 * Lista de categorias
 */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ScrollView, StatusBar, Text, View } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { Creators as CategoriesActions } from '../../store/ducks/categories'
import { Header, CategoryArticlesCarousel } from '../../components'

import styles from './styles'

class ListCategory extends Component {
  state = {
    hasCategories: false
  }

  static propTypes = {
    setCategories: PropTypes.func,
    categories: PropTypes.shape({
      listCategory: PropTypes.array
    })
  }

  constructor(props) {
    super(props)
    this.listCategory = props.categories.listCategory
  }

  componentDidMount() {
    this.getCategories()
    this.toggleDrawer()
    this.hasCategories()
  }

  // Verifica se lista de categorias não está vazia e apta a renderizar
  hasCategories = () => {
    let hasCategories = false

    if (this.props.categories.listCategory)
      this.props.categories.listCategory.forEach(category =>
        category.items.length ? hasCategories = true : hasCategories
      )

    this.setState({ hasCategories })
  }

  toggleDrawer = () => {
    StatusBar.setBarStyle('light-content', true)
    this.props.navigation.toggleDrawer()
  }

  // Requisita o json disponilizado no repositório
  getCategories = async () => {
    try {
      if (!this.props.categories.listCategory.length) {
        let listCategory = await fetch(
          'https://idx.digital/static/idx-teste-de-nivel/mobile-test-one.json'
        )

        listCategory = await listCategory.json()

        /**
         * Percorre a lista e adiciona, e inicializa,
         * as propriedades extras de id e favorite
         */
        listCategory.map(category =>
          category.items.map((item, id) => {
            item.id = id
            item.favorite = false
          })
        )

        // Seta as categorias a partir do Redux
        this.props.setCategories(listCategory)
      }
    } catch (error) {
      this.setState({ hasCategories: false })
    }
  }

  openArticle = (category, item) => {
    StatusBar.setBarStyle('light-content', true)
    this.props.navigation.navigate('Article', {
      category,
      article: item
    })
  }

  renderCarousels = () =>
    this.props.categories.listCategory.map((category, id) => (
      <View key={id} style={styles.categoryContainer}>
        <Text style={styles.categoryText}>{category.category}</Text>
        <CategoryArticlesCarousel
          category={category.category}
          items={category.items}
          openArticle={this.openArticle}
        />
      </View>
    ))

  render() {
    return (
      <View style={styles.container}>
        <Header title="Home" onPress={this.toggleDrawer} />

        <ScrollView>
          {/* Verificação e renderização da lista de categorias */}
          {!this.state.hasCategories ? (
            <Text style={styles.notFoundText}>
              Conteúdo não carregado, cheque sua conexão
            </Text>
          ) :
            this.renderCarousels()
          }
        </ScrollView>
      </View>
    )
  }
}

// Mapeamento das props do redux
const mapStateToProps = state => {
  const { categories } = state

  return { categories }
}

// Mapeamento das actions do redux
const mapDispatchToProps = dispatch =>
  bindActionCreators(CategoriesActions, dispatch)

const _ListCategory = connect(
  mapStateToProps,
  mapDispatchToProps
)(ListCategory)

export { _ListCategory as ListCategory }
