/**
 * Componente de Menu usado no drawer
 */
import React, { Component } from 'react'
import {
  ImageBackground,
  StatusBar,
  Text,
  TouchableOpacity,
  View
} from 'react-native'

import styles from './styles'

const indexBackground = require('../../assets/index-digital.png')

class Menu extends Component {
  openListCategory = () => {
    this.props.navigation.navigate('ListCategory')
    this.props.navigation.closeDrawer()
    StatusBar.setBarStyle('dark-content', true)
  }

  openListFavorite = () => {
    this.props.navigation.navigate('ListFavorite')
    this.props.navigation.closeDrawer()
    StatusBar.setBarStyle('dark-content', true)
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          source={indexBackground}
          style={styles.indexBackground}
        >
          <Text style={styles.appTitle}>IDX</Text>
        </ImageBackground>

        <View style={styles.options}>
          <TouchableOpacity
            style={styles.optionButton}
            onPress={this.openListCategory}
          >
            <Text style={styles.optionTitle}>Home</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.optionButton}
            onPress={this.openListFavorite}
          >
            <Text style={styles.optionTitle}>Favorites</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

export { Menu }
