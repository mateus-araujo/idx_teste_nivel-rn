import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
  container: {
    flex: 1
  },
  indexBackground: {
    height: 200,
    resizeMode: 'center'
  },
  appTitle: {
    color: '#FFFFFF',
    fontSize: 25,
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 150,
    marginLeft: 20
  },
  options: {
    marginTop: 20
  },
  optionButton: {
    paddingHorizontal: 20,
    paddingVertical: 10
  },
  optionTitle: {
    fontSize: 25,
    fontFamily: 'Montserrat-Light'
  }
})
