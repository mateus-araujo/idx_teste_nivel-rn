import React, { Component, Fragment } from 'react'
import { ActivityIndicator, StatusBar, View } from 'react-native'
import SplashScreen from 'react-native-splash-screen'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'

import { persistor, store } from './store'
import NavigationService from './services/NavigationService'
import Routes from './routes'
import './ReactotronConfig'

// Componente de loading exigido pelo Persist Gate
const Loading = () => (
  <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
    <ActivityIndicator size="large" color="#EC008C" />
  </View>
)

class App extends Component {
  // Esconde a splash screen configurada
  componentDidMount() {
    SplashScreen.hide()
  }

  render() {
    return (
      // Retorno do componente com configurações de redux, status bar...
      <Provider store={store}>
        <PersistGate loading={<Loading />} persistor={persistor}>
          <Fragment>
            <StatusBar
              translucent
              backgroundColor="rgba(0,0,0,0)"
              barStyle="light-content"
            />
            <Routes
              ref={navigatorRef => {
                NavigationService.setTopLevelNavigator(navigatorRef)
              }}
            />
          </Fragment>
        </PersistGate>
      </Provider>
    )
  }
}

export default App
