/**
 * Duck Pattern no Redux para maior simplificação
 */
import { createActions, createReducer } from 'reduxsauce'

export const { Types, Creators } = createActions({
  setCategories: ['payload'],
  setFavorite: ['category', 'id']
})

const INITIAL_STATE = {
  listCategory: [],
  listFavorite: [],
  hasFavorites: false
}

const setCategories = (state = INITIAL_STATE, action) => ({
  ...INITIAL_STATE,
  ...state,
  listCategory: action.payload
})

// Seta o favorito e realiza demais alterações
const setFavorite = (state = INITIAL_STATE, action) => {
  // Percorre a lista de categorias para fazer o toggle no item.favorite do article
  state.listCategory.map(category => {
    category.category === action.category
      ? category.items.map(item =>
        item.id === action.id ? item.favorite = !item.favorite : item
      )
      : category
  })

  /**
   * Copia lista de categorias para a de favoritos e realiza o
   * filtro com apenas os items marcados como favoritos
   */
  state.listFavorite = state.listCategory
  state.listFavorite = state.listFavorite.map(category => ({
    ...category,
    items: category.items.filter(item => item.favorite ? item : null)
  }))

  /**
   * Verifica se existem itens marcados como favoritos, para renderizar
   * apenas se houver, senão é mostrado uma mensagem no componente ListFavorite
   */
  let hasFavorites = false
  state.listCategory.forEach(category =>
    category.items.forEach(item =>
      item.favorite ? hasFavorites = true : null
    )
  )

  return {
    ...INITIAL_STATE,
    listCategory: state.listCategory,
    listFavorite: state.listFavorite,
    hasFavorites: hasFavorites
  }
}

export default createReducer(INITIAL_STATE, {
  [Types.SET_CATEGORIES]: setCategories,
  [Types.SET_FAVORITE]: setFavorite
})
